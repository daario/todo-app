import angular from 'angular';
import uiRouter from 'angular-ui-router';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import './styles/app.scss';
// import { CommonModule } from './common/common.module';
// import './styles/app.scss';

export default angular.module('app',
    [   
        ComponentsModule,
        // CommonModule,
        uiRouter
    ]
)
.component('quoteApp', AppComponent)
.name;
