import angular from 'angular';
// import { InstagramFeedModule } from './instagramfeed/instagramfeed.module';
import { QuoteModule } from './quote/quote.module';


export const ComponentsModule = angular.module('app.components',
    [
        QuoteModule
    ])
    .name;