export const QuoteComponent = {
    template: `
    <div class="quote-text-container">
        <p>&#10077; {{$ctrl.quotes.quote}}</p>
        <span>- {{$ctrl.quotes.author}}</span>
    </div>
    <div class="quote-submit-container">
        <input type="button" value="New Quote" ng-click="$ctrl.nextQuote();">
    </div>   
    `,
    controller: class QuoteComponent {
        constructor(QuoteService) {
            'ngInject';
            this.QuoteService = QuoteService;
        }
        $onInit() {
            this.quotes = [];
            this.QuoteService.getQuotes().then(response => this.quotes = response);
          }
        nextQuote() {
            console.log(this.quotes);
            this.QuoteService.getQuotes().then(response => this.quotes = response);
        }  
    }
};