import angular from 'angular';
import uiRouter from 'angular-ui-router'; 
import { QuoteComponent } from './Quote.component';
import { QuoteService } from './Quote.service';


export const QuoteModule = angular
    .module('quote',
    [
        uiRouter
    ]
    )
    .component('quoteCard', QuoteComponent)
    .service('QuoteService', QuoteService)
    .config(($stateProvider, $locationProvider, $urlRouterProvider) => {
        'ngInject';
        $stateProvider
            .state('quote', {
                url: '/',
                component: 'quoteCard'
            });
        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);
    })
    .name;