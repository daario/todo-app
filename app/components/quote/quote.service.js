/* ----- quote/quote.service.js ----- */
export class QuoteService {
    constructor($http) {
      'ngInject';
      this.$http = $http;
    }
    getQuotes() {
        const customeHeaders = {
            'X-Mashape-Key': 'RwPR1vk8PSmshxOYrUV65ul00MGUp1Th9qKjsnFNbMkSBqMOdZ',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
        }
        return this.$http.get('https://andruxnet-random-famous-quotes.p.mashape.com/?cat=famous&count=1', {headers: customeHeaders}).then(response => response.data);
    }
  }