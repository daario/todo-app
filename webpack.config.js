const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
// const path = require('path');

const is_prod = process.env.NODE_ENV === 'production';
const css_dev = ['style-loader', 'css-loader?sourceMap', 'sass-loader'];
const css_prod = ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
        'css-loader',
        'sass-loader'
    ],
    publicPath: '/dist',
});

const css_config = (is_prod) ? css_prod : css_dev;

module.exports = {
    context: __dirname + '/app',
    entry: './',
    output: {
        path: __dirname + '/app',
        filename: 'app.bundle.js'
    },
    devServer: {
        port: 3060,
        host: '192.168.40.84',
        contentBase: __dirname + '/app'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use:{
                    loader: 'babel-loader',
                    options:{
                        presets:[
                            'env'
                        ],
                        plugins:[
                            'add-module-exports'
                        ]
                    }
                }
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use:[
                    'file-loader?name=images/[name].[ext]',
                    // 'file-loader?name=[name].[ext]&outputPath=images/&publicPath=images/'
                    'image-webpack-loader'
                ]
            },
            {
                test: /\.html$/,
                exclude: /(node_modules|bower_components)/,
                use:{
                    loader: 'raw-loader'
                }
            },
            {
                test: /\.scss$/,
                exclude: /(node_modules|bower_components)/,
                use: css_config
            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin(
            {
                // minify: {
                //     collapseWhitespace: true,
                // },
                hash: true,
                template: './index.html',
            }),
        new ExtractTextPlugin(
            {
                filename: 'app.css',
                disable: !css_config,
                allChunks: true,
            }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
    ]
}